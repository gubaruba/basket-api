# Basket API Documentation #

## API Root Endpoint ##


```
#!php

http://basket-api.jazzz.jq/v1
```

## Methods ##

Description|Method|Path
-----------|------|----
[Retrieve list of all baskets](Retrieve list of all baskets)|GET|/baskets
[Create a new basket](Create a new basket)|POST|/baskets
[Update an existing basket](Update an existing basket)|PUT|/baskets/{id}
[Delete a basket](Delete a basket)|DELETE|/baskets/{id}
[Get items from a basket](Get items from a basket)|GET|/baskets/{id}/items
[Add items into the basket](Add items into the basket)|POST|/baskets/{id}/items
[Delete item from the basket](Delete item from the basket)|DELETE|/baskets/{id}/items/{itemId}
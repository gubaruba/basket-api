<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * Sends response when exception was thrown
     *
     * @param string $message
     * @param integer $code
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function respondException($message, $code)
    {
        return response()->json(['status' => 'error', 'message' => $message], $code);
    }
}

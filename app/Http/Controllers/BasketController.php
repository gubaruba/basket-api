<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Support\Facades\Validator;

use App\Models\ModelHelper;
use App\Models\Basket;
use App\Models\Item;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Controller for basket operations
 *
 * @author Volodymyr Lalykin
 */
class BasketController extends Controller
{
    /**
     * Lists all baskets
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        try {
            return response()->json(ModelHelper::getAllModels('App\Models\Basket'));
        } catch (HttpException $e) {
            return $this->respondException($e->getMessage(), $e->getStatusCode());
        }
    }

    /**
     * Creates a particular model
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), Basket::$rules);

            if ($validator->fails()) {
                return $this->respondException($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            $model = new Basket();
            $model->setAttributes($request->all());

            if ($model->save()) {
                return response()->json($model->output(), Response::HTTP_CREATED);
            }
        } catch (HttpException $e) {
            return $this->respondException($e->getMessage(), $e->getStatusCode());
        }
    }

    /**
     * Updates the particular basket
     *
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        try {
            $model = ModelHelper::getModelById('App\Models\Basket', $id);

            $validator = Validator::make($request->all(), Basket::$rules);

            if ($validator->fails()) {
                return $this->respondException($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            $model->setAttributes($request->all());

            if ($model->overloaded($model->getItems())) {
                return $this->respondException('Overloaded', Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            if ($model->save()) {
                return response()->json($model->output());
            }
        } catch (HttpException $e) {
            return $this->respondException($e->getMessage(), $e->getStatusCode());
        }
    }

    /**
     * Deletes basket by id
     *
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function delete(Request $request, $id)
    {
        try {
            $model = ModelHelper::getModelById('App\Models\Basket', $id);
            if ($model->delete()) {
                return response()->json($model->output());
            }
        } catch (HttpException $e) {
            return $this->respondException($e->getMessage(), $e->getStatusCode());
        }
    }

    /**
     * Retrieves basket with a list of its items
     *
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function listItems(Request $request, $id)
    {
        try {
            $model = ModelHelper::getModelById('App\Models\Basket', $id);
            return response()->json($model->output());
        } catch (HttpException $e) {
            return $this->respondException($e->getMessage(), $e->getStatusCode());
        }
    }

    /**
     * Adds items into the basket
     *
     * @param Request $request
     * @param integer $id basket id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addItems(Request $request, $id)
    {
        try {
            $model = ModelHelper::getModelById('App\Models\Basket', $id);

            $errors = $model->validateItems($request->input('item'));

            if ($errors) {
                return $this->respondException($errors, Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            $items = [];
            foreach ($request->input('item') as $k => $value) {
                $item = new Item();
                $item->setAttributes((array)json_decode($value));
                $items[] = $item;
            }

            if ($model->overloaded($items)) {
                return $this->respondException('Overloaded', Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            if ($model->add($items)) {
                return response()->json($model->output());
            }

        } catch (HttpException $e) {
            return $this->respondException($e->getMessage(), $e->getStatusCode());
        }
    }

    /**
     * Removes item from the basket
     *
     * @param Request $request
     * @param integer $id basket id
     * @param integer $itemId item id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function removeItems(Request $request, $id, $itemId)
    {
        try {
            $model = ModelHelper::getModelById('App\Models\Basket', $id);
            $item = ModelHelper::getModelById('App\Models\Item', $itemId);

            if ($model->remove($item)) {
                return response()->json($model->output());
            }

        } catch (HttpException $e) {
            return $this->respondException($e->getMessage(), $e->getStatusCode());
        }
    }
}
<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->welcome();
});

$app->group(['prefix' => 'v1', 'namespace' => 'App\Http\Controllers'], function ($app) {
    $app->get('baskets', ['uses' => 'BasketController@index']);
    $app->post('baskets', ['uses' => 'BasketController@create']);
    $app->put('baskets/{id}', ['uses' => 'BasketController@update']);
    $app->delete('baskets/{id}', ['uses' => 'BasketController@delete']);

    $app->get('baskets/{id}/items', ['uses' => 'BasketController@listItems']);
    $app->post('baskets/{id}/items', ['uses' => 'BasketController@addItems']);
    $app->delete('baskets/{id}/items/{itemId}', ['uses' => 'BasketController@removeItems']);
});

<?php
namespace App\Models;

use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Support\Facades\Validator;

/**
 * Basket model class
 *
 * @author Volodymyr Lalykin
 */
class Basket extends Model
{
    /**
     * @var integer id
     */
    public $id;

    /**
     * @var string name
     */
    public $name;

    /**
     * @var integer capacity
     */
    public $capacity;

    /**
     * @var string content, item ids separated by comma
     */
    public $content = '';

    /**
     * @var array set of items
     */
    private $items;

    public $overloaded = false;

    /**
     * @inheritdoc
     */
    public $attributes = [
        'id',
        'name',
        'capacity',
        'content',
    ];

    /**
     * @inheritdoc
     */
    public $safe = [
        'name',
        'capacity',
    ];

    /**
     * Array of model validation rules
     *
     * @var string[]
     */
    public static $rules = [
        'name' => 'required|max:255',
        'capacity' => 'required|integer|min:1',
        'content' => ['regex:#^(\d[\d\,]*\d|\d)$#'],
    ];

    /**
     * @inheritdoc
     */
    public function getTableName()
    {
        return 'baskets';
    }

    /**
     * @inheritdoc
     */
    public function getModelName()
    {
        return 'Basket';
    }

    /**
     * Returns basket as array prepared for response
     *
     * @return mixed[]
     */
    public function output()
    {
        $items = [];
        foreach ($this->getItems() as $item) {
            $items[] = $item->output();
        }
        return array_merge($this->getAttributes(), ['weight' => $this->getCurrentWeight(), 'items' => $items]);
    }

    /**
     * Returns array of items
     *
     * @return Item[]
     */
    public function getItems()
    {
        if ($this->items == null) {
            $items = DB::table('items')->whereIn('id', $this->getItemIds())->get();
            $this->items = [];
            foreach ($items as $attributes) {
                $item = new Item();
                $item->setAttributes((array)$attributes, false);
                $this->items[] = $item;
            }
        }
        return $this->items;
    }

    /**
     * Sets item property
     *
     * @param Item[] $items
     */
    public function setItems($items)
    {
        $this->items = $items;
    }

    /**
     * Returns item ids by content attribute
     *
     * @return integer[]
     */
    public function getItemIds()
    {
        return $this->content ? explode(',', $this->content) : [];
    }

    /**
     * Checks if basket is overloaded
     *
     * @param Item[] $items
     * @return boolean
     */
    public function overloaded($items)
    {
        return ($this->getCurrentWeight() + $this->getSummaryWeight($items)) > $this->capacity;
    }

    /**
     * Return basket current weight
     *
     * @return integer
     */
    public function getCurrentWeight()
    {
        return $this->getSummaryWeight($this->getItems());
    }

    /**
     * Return items summary weight
     *
     * @param Item[] $items
     * @return integer
     */
    public function getSummaryWeight($items)
    {
        $summary = 0;
        foreach ($items as $item) {
            $summary += $item->weight;
        }
        return $summary;
    }

    /**
     * Validates item array and return errors
     *
     * @param Item[] $items
     * @return array
     */
    public function validateItems($items)
    {
        $errors = [];
        foreach ($items as $k => $item) {
            $validator = Validator::make((array)json_decode($item), Item::$rules);

            if ($validator->fails()) {
                $errors[$k] = $validator->errors();
            }
        }
        return $errors;
    }

    /**
     * Removes items from the basket
     *
     * @param Item[] $items
     * @return boolean whether the update was successful
     */
    public function remove($items)
    {
        $items = is_array($items) ? $items : [$items];
        $itemIds = $this->getItemIds();
        foreach ($items as $item) {
            if ($item->delete()) {
                foreach ($itemIds as $k => $id) {
                    if ($id == $item->id) {
                        unset($itemIds[$k]);
                        break;
                    }
                }
            }
        }
        $this->content = implode(',', $itemIds);
        return $this->save();
    }

    /**
     * Adds items into the basket
     *
     * @param Item[] $items
     * @return boolean whether the update was successful
     */
    public function add($items)
    {
        $items = is_array($items) ? $items : [$items];
        $itemIds = [];
        foreach ($items as $item) {
            if ($item->save()) {
                $itemIds[] = $item->id;
            }
        }

        if ($itemIds) {
            $this->setItems(array_merge($this->getItems(), $items));
            $content = array_merge($this->getItemIds(), $itemIds);
            $this->content = implode(',', $content);
            return $this->save();
        }
        return true;
    }

    /**
     * Deletes basket with its content
     *
     * @return boolean whether deleting was successful
     */
    public function delete()
    {
        if (DB::table($this->getTableName())->where('id', $this->id)->delete()) {
            foreach ($this->getItems() as $item) {
                $item->delete();
            }
            return true;
        }
        return false;
    }
}
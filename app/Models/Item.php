<?php
namespace App\Models;

use Illuminate\Support\Facades\DB;

/**
 * Basket item model class
 *
 * @author Volodymyr Lalykin
 */
class Item extends Model
{
    /**
     * @var integer id
     */
    public $id;

    /**
     * @var string type
     */
    public $type;

    /**
     * @var integer weight
     */
    public $weight;

    /**
     * @inheritdoc
     */
    public $attributes = [
        'id',
        'type',
        'weight',
    ];

    /**
     * @inheritdoc
     */
    public $safe = [
        'type',
        'weight'
    ];

    /**
     * @inheritdoc
     */
    public static $rules = [
        'type' => 'required|max:255',
        'weight' => 'required|integer',
    ];

    /**
     * @inheritdoc
     */
    public function getTableName()
    {
        return 'items';
    }

    /**
     * @inheritdoc
     */
    public function getModelName()
    {
        return 'Item';
    }
}
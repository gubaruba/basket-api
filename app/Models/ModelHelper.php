<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

/**
 * Helper for model operations
 *
 * @author Volodymyr Lalykin
 */
class ModelHelper
{
    /**
     * Returns model by its class name and id
     *
     * @param string $className
     * @param integer $id
     * @return Model
     */
    public static function getModelById($className, $id)
    {
        if (!class_exists($className)) {
            abort(400, 'Class does not exist.');
        }

        $model = new $className;
        $model->setIsNew(false);

        $data = DB::table($model->getTableName())->where('id', $id)->first();

        if (!$data) {
            abort(404, $model->getModelName() . ' #' . $id . ' not found.');
        }

        $model->setAttributes((array)$data, false);

        return $model;
    }

    /**
     * Returns an array of models by class name
     *
     * @param string $className
     * @return stdClass[]
     */
    public static function getAllModels($className)
    {
        if (!class_exists($className)) {
            abort(400, 'Class does not exist.');
        }

        return DB::table((new $className)->getTableName())->get();
    }
}
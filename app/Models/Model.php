<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

/**
 * Abstract class for models
 *
 * @author Volodymyr Lalykin
 */
abstract class Model
{
    /**
     * Array of attribute names
     *
     * @var string[]
     */
    public $attributes = [];

    /**
     * Array of attribute names which can be safely assigned
     *
     * @var array
     */
    public $safe = [];

    /**
     * Whether the model is new
     *
     * @var boolean
     */
    protected $new = true;

    /**
     * Returns table name
     *
     * @return string
     */
    abstract public function getTableName();

    /**
     * Returns human-friendly model name
     *
     * @return string
     */
    abstract public function getModelName();

    /**
     * Sets "new" property
     *
     * @param boolean $new
     */
    public function setIsNew($new = true)
    {
        $this->new = $new;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Returns model as array prepared for response
     *
     * @return mixed[]
     */
    public function output()
    {
        return $this->getAttributes();
    }

    /**
     * Sets model attributes with passed values
     *
     * @param mixed[] $values
     */
    public function setAttributes($values, $safe = true)
    {
        if (!is_array($values)) {
            return;
        }

        $attributes = array_flip($safe ? $this->safe : $this->attributes);

        foreach ($values as $name => $value) {
            if (isset($attributes[$name])) {
                $this->$name = $value;
            }
        }
    }

    /**
     * Returns array of model attributes
     *
     * @return string[]
     */
    public function getAttributes()
    {
        $attributes = [];
        foreach ($this->attributes as $attribute) {
            $attributes[$attribute] = $this->$attribute;
        }

        return $attributes;
    }

    /**
     * Inserts the model record into the database
     *
     * @return boolean whether insert was successful
     */
    public function insert()
    {
        $id = DB::table($this->getTableName())->insertGetId($this->getAttributes());

        if ($id) {
            $this->setId($id);
            return true;
        }
        return false;
    }

    /**
     * Updates current model record at the database
     *
     * @return boolean whether update was successful
     */
    public function update()
    {
        if (DB::table($this->getTableName())->where('id', $this->id)->update($this->getAttributes())) {
            return true;
        }

        //Check if data not changed
        $stored = (array)DB::table($this->getTableName())->where('id', $this->id)->first();

        if ($stored == $this->getAttributes()) {
            return true;
        }

        return false;
    }

    /**
     * Saves current model
     *
     * @return boolean whether saving was successful
     */
    public function save()
    {
        if ($this->new) {
            return $this->insert();
        }
        return $this->update();
    }

    /**
     * Deletes current model
     *
     * @return boolean whether the record was successfully deleted
     */
    public function delete()
    {
        return DB::table($this->getTableName())->where('id', $this->id)->delete();
    }
}